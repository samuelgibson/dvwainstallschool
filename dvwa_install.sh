#/bin/bash
echo -e "\n#######################################"
echo -e "# Damn Vulnerable Web App Installer Script #"
echo -e "#######################################"
echo -n "[*] Starting MySQL..."
service mysql start
echo -e "Done!\n"

echo -e -n "\n[*] Changing directory to /var/www/html..."
cd /var/www/html > /dev/null
echo -e "Done!\n"

echo -n "[*] Starting Web Service..."
service apache2 start
echo -e "Done!\n"

echo -n "[*] Removing default index.html..."
rm index.html > /dev/null
echo -e "Done!\n"

echo -n "[*] Changing to Temp Directory..."
cd /tmp
echo -e "Done!\n"

echo "[*] Downloading DVWA..."
wget https://github.com/ethicalhack3r/DVWA/archive/master.zip
echo -e "Done!\n"

echo -n "[*] Unzipping DVWA..."
unzip master.zip > /dev/null
echo -e "Done!\n"

echo -n "[*] Deleting the zip file..."
rm master.zip > /dev/null
echo -e "Done!\n"

echo -n "[*] Copying dvwa to root of Web Directory..."
cp -R DVWA-master/* /var/www/html > /dev/null
echo -e "Done!\n"

echo -n "[*] Clearing Temp Directory..."
rm -R DVWA-master > /dev/null
echo -e "Done!\n"

echo -n "[*] Enabling Remote include in php.ini..."
cp /etc/php/7.0/apache2/php.ini /etc/php/7.0/apache2/php.ini1
sed -e 's/allow_url_include = Off/allow_url_include = On/' /etc/php/7.0/apache2/php.ini1 > /etc/php/7.0/apache2/php.ini
rm //etc/php/7.0/apache2/php.ini1
echo -e "Done!\n"

echo -n "[*] Enabling write permissions to /var/www/hackable/upload..."
chmod 777 /var/www/html/hackable/uploads/
chmod 777 /var/www/html/external/phpids/0.6/lib/IDS/tmp/phpids_log.txt
echo -e "Done!\n"

echo -n "[*] Creating New User..."
mysql -u root --password='' -e 'DROP DATABASE IF EXISTS dvwa;'
mysql -u root --password='' -e 'CREATE DATABASE dvwa;'
mysql -u root --password='' -e 'CREATE USER "user"@"127.0.0.1" IDENTIFIED BY "password";'
mysql -u root --password='' -e 'GRANT ALL on dvwa.* TO "user"@"127.0.0.1";'
mysql -u root --password='' -e 'FLUSH PRIVILEGES;'
echo -e "Done!\n"

echo -n "[*] Updating Config File..."
sed -e 's/root/user/' -e 's/p@ssw0rd/password/' /var/www/html/config/config.inc.php.dist > /var/www/html/config/config.inc.php
rm /var/www/html/config/config.inc.php.dist
echo -e "Done!\n"

echo -n "[*] Installing php module gd..."
apt-get install php7.0-gd -y
service apache2 restart
echo -e "Done!\n"

echo -e -n "[*] Starting Firefox to DVWA\nUserName: admin\nPassword: password\n"
echo -e -n "[*] Click Clear / Reset Database. Recaptcha codes can be found at https://www.google.com/recaptcha/admin"
firefox http://127.0.0.1/login.php &> /dev/null &
echo -e "\nDone!\n"
echo -e "[\033[1;32m*\033[1;37m] DVWA Install Finished!\n"